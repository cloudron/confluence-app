On first visit, you will see a wizard to add a license key. Use the database, LDAP and SMTP
credentials stored in `/app/data/credentials.txt` to finish the installation.

