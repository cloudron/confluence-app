### About

Atlassian Confluence is where you create, organize and discuss work with your team.

Capture the knowledge that's too often lost in email inboxes and shared
network drives in Confluence instead – where it's easy to find, use, and
update. Give every team, project, or department its own space to create
the things they need, whether it's meeting notes, product requirements,
file lists, or project plans, you can get it done in Confluence.
