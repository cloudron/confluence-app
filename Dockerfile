FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ENV DEBIAN_FRONTEND noninteractive

# https://adoptium.net/blog/2021/12/eclipse-temurin-linux-installers-available/
RUN wget -qO - https://packages.adoptium.net/artifactory/api/gpg/key/public | apt-key add -
RUN apt-get update -y && \
    apt-get install -y software-properties-common xmlstarlet libtcnative-1 && \
    add-apt-repository --yes 'https://packages.adoptium.net/artifactory/deb jammy main' && \
    apt-get update -y && \
    apt-get install -y temurin-11-jdk && \
    rm -r /var/cache/apt /var/lib/apt/lists

ENV CONFLUENCE_INSTALL /home/cloudron/confluence

# https://www.atlassian.com/software/confluence/download
ARG VERSION=8.5.15

RUN mkdir -p "${CONFLUENCE_INSTALL}/conf" \
    && curl -Ls "https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${VERSION}.tar.gz" | tar -xz --directory "${CONFLUENCE_INSTALL}" --strip-components=1 --no-same-owner

RUN echo -e "\nconfluence.home=/app/data" >> "${CONFLUENCE_INSTALL}/confluence/WEB-INF/classes/confluence-init.properties"

RUN xmlstarlet ed --inplace \
        --delete "Server/@debug" \
        --delete "Server/Service/Connector/@debug" \
        --delete "Server/Service/Connector/@useURIValidationHack" \
        --delete "Server/Service/Connector/@minProcessors" \
        --delete "Server/Service/Connector/@maxProcessors" \
        --delete "Server/Service/Engine/@debug" \
        --delete "Server/Service/Engine/Host/@debug" \
        --delete "Server/Service/Engine/Host/Context/@debug" \
        "${CONFLUENCE_INSTALL}/conf/server.xml"

RUN mv "${CONFLUENCE_INSTALL}/conf" "${CONFLUENCE_INSTALL}/conf.template"

RUN rm -rf ${CONFLUENCE_INSTALL}/temp ${CONFLUENCE_INSTALL}/logs ${CONFLUENCE_INSTALL}/work \
    && ln -s /tmp/confluence "${CONFLUENCE_INSTALL}/temp" \
    && ln -s /run/confluence/logs "${CONFLUENCE_INSTALL}/logs" \
    && ln -s /run/confluence/work "${CONFLUENCE_INSTALL}/work" \
    && ln -s /run/confluence/conf "${CONFLUENCE_INSTALL}/conf"

COPY start.sh credentials.txt.template confluence.cfg.xml.template /home/cloudron/

CMD [ "/home/cloudron/start.sh" ]
