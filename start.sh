#!/bin/bash

set -eu

# https://confluence.atlassian.com/doc/confluence-home-and-other-important-directories-590259707.html
# there is another cfg.xml in shared-home (which is "shared" across all nodes in datacenter edition)
if [[ -f "/app/data/confluence.cfg.xml" ]]; then
    echo "Updating database settings"
    # this only updates and won't upsert because confluence will crash if it didn't set the values itself
    xmlstarlet ed --inplace \
        --update "//confluence-configuration/properties/property[@name='hibernate.connection.username']" -v "${CLOUDRON_POSTGRESQL_USERNAME}" \
        --update "//confluence-configuration/properties/property[@name='hibernate.connection.password']" -v "${CLOUDRON_POSTGRESQL_PASSWORD}" \
        --update "//confluence-configuration/properties/property[@name='hibernate.connection.url']" -v "jdbc:postgresql://${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}" \
        /app/data/confluence.cfg.xml
fi

# These values can never change post installation...
# in the future, maybe we can automate the LDAP and SMTP configuration by editing the db post installation
# https://jira.atlassian.com/browse/CONFSERVER-29033
sed -e "s/CLOUDRON_POSTGRESQL_HOST/${CLOUDRON_POSTGRESQL_HOST}/" \
    -e "s/CLOUDRON_POSTGRESQL_PORT/${CLOUDRON_POSTGRESQL_PORT}/" \
    -e "s/CLOUDRON_POSTGRESQL_DATABASE/${CLOUDRON_POSTGRESQL_DATABASE}/" \
    -e "s/CLOUDRON_POSTGRESQL_USERNAME/${CLOUDRON_POSTGRESQL_USERNAME}/" \
    -e "s/CLOUDRON_POSTGRESQL_PASSWORD/${CLOUDRON_POSTGRESQL_PASSWORD}/" \
    -e "s/CLOUDRON_LDAP_SERVER/${CLOUDRON_LDAP_SERVER}/" \
    -e "s/CLOUDRON_LDAP_PORT/${CLOUDRON_LDAP_PORT}/" \
    -e "s/CLOUDRON_LDAP_BIND_DN/${CLOUDRON_LDAP_BIND_DN}/" \
    -e "s/CLOUDRON_LDAP_BIND_PASSWORD/${CLOUDRON_LDAP_BIND_PASSWORD}/" \
    -e "s/CLOUDRON_LDAP_USERS_BASE_DN/${CLOUDRON_LDAP_USERS_BASE_DN}/" \
    -e "s/CLOUDRON_MAIL_FROM/${CLOUDRON_MAIL_FROM}/" \
    -e "s/CLOUDRON_MAIL_SMTP_SERVER/${CLOUDRON_MAIL_SMTP_SERVER}/" \
    -e "s/CLOUDRON_MAIL_SMTP_PORT/${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/CLOUDRON_MAIL_SMTP_USERNAME/${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/CLOUDRON_MAIL_SMTP_PASSWORD/${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    /home/cloudron/credentials.txt.template > /app/data/credentials.txt

echo "Creating directories"
rm -rf /run/confluence /tmp/confluence && mkdir -p /run/confluence/{conf,logs,work} /tmp/confluence
cp -r /home/cloudron/confluence/conf.template/* /run/confluence/conf
rm -f /app/data/lock

echo "Setting up reverse proxy config"
xmlstarlet ed --inplace \
    --insert "/Server/Service/Connector" --type attr -n proxyName -v "${CLOUDRON_APP_DOMAIN}" \
    --insert "/Server/Service/Connector" --type attr -n proxyPort -v "443" \
    --insert "/Server/Service/Connector" --type attr -n scheme -v "https" \
    --insert "/Server/Service/Connector" --type attr -n secure -v "true" \
    /run/confluence/conf/server.xml

echo "Fixing ownership"
chown -R cloudron:cloudron /app/data /run/confluence /tmp/confluence

echo "Starting confluence"
cd ${CONFLUENCE_INSTALL}
exec /usr/local/bin/gosu cloudron:cloudron ${CONFLUENCE_INSTALL}/bin/catalina.sh run

